/* global FIXTURE, TEST */
var NI = require('tkjs-ni');
var MODEL = require('./model');

FIXTURE("MODEL", function() {

  TEST("basic", function() {
    var modelObj = MODEL.registerModel("test_module:get_info", {
      id: MODEL.String
    });
    NI.assert.isTrue(MODEL.getModel("test_module:get_info"));

    NI.assert.isTrue(MODEL.validate({ id: "foo" }, "test_module:get_info").isValid());
    NI.assert.isTrue(MODEL.validate({ id: "foo" }, modelObj).isValid());

    NI.assert.isFalse(MODEL.validate({ id: 123 }, "test_module:get_info").isValid());
    NI.assert.isFalse(MODEL.validate({ id: {} }, "test_module:get_info").isValid());
    NI.assert.isFalse(MODEL.validate({ id_nope: "foo" }, "test_module:get_info").isValid());
  });

});
